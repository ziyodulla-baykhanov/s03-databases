/** Task 1. Create a new user with the username "rentaluser" 
*   and the password "rentalpassword". Give the user the ability 
*   to connect to the database but no other permissions.
*/
-- Drop the user if it exists (if necessary)
DROP USER IF EXISTS "rentaluser";

-- Create the user
CREATE USER "rentaluser" WITH PASSWORD 'rentalpassword';

-- Revoke all permissions from the user
REVOKE ALL ON DATABASE dvdrental FROM "rentaluser";

-- Grant the CONNECT permission for the user
GRANT CONNECT ON DATABASE dvdrental TO "rentaluser";

-- Check that CONNECT permission is created
SELECT has_database_privilege('rentaluser', 'dvdrental', 'CONNECT');



/** Task 2. Grant "rentaluser" SELECT permission for the "customer" table. 
*   Сheck to make sure this permission works correctly, 
*   write a SQL query to select all customers.
*/
-- Grant the SELECT permission for the "customer" table
GRANT SELECT ON TABLE customer TO rentaluser;

-- Check the permission:
-- Switch to rentaluser
SET ROLE rentaluser;

-- Select all customers - this should work CORRECTLY:
SELECT * FROM customer;

-- Select all rental records - this should FAIL:
-- SELECT * FROM rental;

-- Switch back to the original role
RESET ROLE;



/** Task 3. Create a new user group called "rental" and 
*   add "rentaluser" to the group.  
*/
-- Drop the user group if it exists (if necessary)
DROP ROLE IF EXISTS rental;

-- Creat the user group
CREATE ROLE rental;

-- Add the user to the group
GRANT rental TO rentaluser;



/** Task 4. Grant the "rental" group INSERT and UPDATE permissions 
*   for the "rental" table. Insert a new row and update 
*   one existing row in the "rental" table under that role.
*/
-- Grant the INSERT and UPDATE permissions for the "rental" table
GRANT INSERT, UPDATE ON TABLE rental TO rental;

-- Check the permissions:
-- Grant additional permissions for checking purposes
GRANT USAGE ON SEQUENCE rental_rental_id_seq TO rental;
GRANT SELECT ON TABLE rental TO rental;

-- Switch to rental group
SET ROLE rental;

-- Insert a new row into the "rental" table with some random values
INSERT INTO rental (
    rental_date, 
    inventory_id, 
    customer_id, 
    return_date, 
    staff_id
    )
VALUES (
    '2005-08-23 22:26:47+05', 
    367, 
    130, 
    '2005-08-27 22:26:47+05', 
    2
    );

-- Updat an existing row
UPDATE rental
SET return_date = '2023-09-28 14:00:00+05'
WHERE rental_date = '2005-08-23 22:26:47+05' 
    AND inventory_id = 367 AND customer_id = 130 AND staff_id = 2;

-- View the changes
SELECT * FROM rental WHERE return_date = '2023-09-28 14:00:00+05';

-- Switch back to the original role
RESET ROLE;

-- Revoke additional permissions given for checking purposes
REVOKE USAGE ON SEQUENCE rental_rental_id_seq FROM rental;
REVOKE SELECT ON TABLE rental FROM rental;



/** Task 5. Revoke the "rental" group's INSERT permission for the "rental" table. 
*   Try to insert new rows into the "rental" table make sure this action is denied.
*/
-- Revoke the INSERT permission for the "rental" table
REVOKE INSERT ON TABLE rental FROM rental;

-- Check the permissions:
-- Switch to rental group
SET ROLE rental;

-- Try to insert a new row into the "rental" table with some random values
INSERT INTO rental (
    rental_date, 
    inventory_id, 
    customer_id, 
    return_date, 
    staff_id
    )
VALUES (
    '2023-11-11 00:01:10', 
    123, 
    111, 
    '2023-11-15 00:01:10', 
    1
    );

-- The result should show an error message like this:
-- ERROR:  permission denied for table rental
-- SQL state: 42501

-- Switch back to the original role
RESET ROLE;


/** Task 6. Create a personalized role for any customer already existing 
*   in the dvdrental database. The name of the role name must be 
*   client_{first_name}_{last_name} (omit curly brackets). 
*   The customer's payment and rental history must not be empty. 
*   Configure that role so that the customer can only access their 
*   own data in the "rental" and "payment" tables. 
*   Write a query to make sure this user sees only their own data.
*/
-- Find 10 customers with non-empty payment and rental history
SELECT c.customer_id, c.first_name, c.last_name, c.email,
    COUNT(r.rental_id) AS rental_count, COUNT(p.payment_id) AS payment_count
FROM customer c
JOIN rental r ON c.customer_id = r.customer_id
JOIN payment p ON c.customer_id = p.customer_id
GROUP BY c.customer_id
HAVING COUNT(r.rental_id) > 0 AND COUNT(p.payment_id) > 0
LIMIT 10;

-- Choose Mary Smith as a user with non-empty payment and rental history
SELECT customer_id, first_name, last_name, email, 
    COUNT(r.rental_id) AS rental_count, COUNT(p.payment_id) AS payment_count
FROM customer
JOIN rental r USING (customer_id)
JOIN payment p USING (customer_id)
WHERE first_name = UPPER('Mary') 
    AND last_name = UPPER('Smith') 
    AND email = 'MARY.SMITH@sakilacustomer.org'
GROUP BY customer_id;

-- Drop the role if it exists (if necessary)
DROP ROLE IF EXISTS client_mary_smith;

-- Create a personalized role for the customer
CREATE ROLE client_mary_smith;

-- Revoke all permissions from the role (if necessary)
REVOKE ALL ON DATABASE dvdrental FROM client_mary_smith;

-- Grant the SELECT permission for the "rental" and "payment" tables
GRANT SELECT ON TABLE rental, payment TO client_mary_smith;

-- Enable Row-Level Security (RLS) on the "rental" and "payment" tables
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;

-- Create RLS policies to restrict access to the user's own data
CREATE POLICY rental_policy
    ON rental
    FOR SELECT TO client_mary_smith
    USING (customer_id = (
        SELECT customer_id FROM customer 
        WHERE first_name = UPPER('Mary') 
            AND last_name = UPPER('Smith') 
            AND email = 'MARY.SMITH@sakilacustomer.org'
        )
    );

CREATE POLICY payment_policy
    ON payment
    FOR SELECT TO client_mary_smith
    USING (customer_id = (
        SELECT customer_id FROM customer 
        WHERE first_name = UPPER('Mary') 
            AND last_name = UPPER('Smith') 
            AND email = 'MARY.SMITH@sakilacustomer.org'
        )
    );

-- Force Row-Level Security (RLS) to be applied
ALTER TABLE rental FORCE ROW LEVEL SECURITY;
ALTER TABLE payment FORCE ROW LEVEL SECURITY;

-- Grant the SELECT permission for the "customer" table for checking purposes
GRANT SELECT ON TABLE customer TO client_mary_smith;

-- Test the personalized role:
SET ROLE client_mary_smith;

-- Should only show the rental records for Mary Smith (customer_id = 1)
SELECT * FROM rental;

-- Should only show the payment records for Mary Smith (customer_id = 1)
SELECT * FROM payment;

-- Switch back to the original role
RESET ROLE;

-- Revoke the SELECT permission for the "customer" table given for checking purposes
REVOKE SELECT ON TABLE customer FROM client_mary_smith;

