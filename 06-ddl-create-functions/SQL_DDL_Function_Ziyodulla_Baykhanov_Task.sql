/** Task 1. Create a view called "sales_revenue_by_category_qtr" 
*   that shows the film category and total sales revenue for the 
*   current quarter. The view should only display categories with 
*   at least one sale in the current quarter. 
*   The current quarter should be determined dynamically.
*/
-- Dropping the view if it exists
DROP VIEW IF EXISTS sales_revenue_by_category_qtr;

-- Creating the view
CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT
    category.category_id,
    category.name AS category_name,
    SUM(payment.amount) AS total_revenue
FROM
    payment
    JOIN rental USING (rental_id)
    JOIN inventory USING (inventory_id)
    JOIN film_category USING (film_id)
    JOIN category USING (category_id)
WHERE
    EXTRACT(YEAR FROM payment.payment_date) = EXTRACT(YEAR FROM CURRENT_DATE)
    AND EXTRACT(QUARTER FROM payment.payment_date) = EXTRACT(QUARTER FROM CURRENT_DATE)
GROUP BY
    category.category_id
HAVING
    SUM(payment.amount) > 0;


-- Checking the view
SELECT * FROM sales_revenue_by_category_qtr;


/** Task 2. Create a query language function called "get_sales_revenue_by_category_qtr" 
*   that accepts one parameter representing the current quarter and returns the same 
*   result as the "sales_revenue_by_category_qtr" view.
*/
-- Dropping the function if it exists
DROP FUNCTION IF EXISTS get_sales_revenue_by_category_qtr(TEXT);

-- Creating the function
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(quarter TEXT)
RETURNS TABLE(
    category_id INTEGER, 
    category_name TEXT,
    total_revenue NUMERIC) 
AS $$
BEGIN
    RETURN QUERY
        SELECT
            category.category_id,
            category.name AS category_name,
            SUM(payment.amount) AS total_revenue
        FROM
            payment
            JOIN rental USING (rental_id)
            JOIN inventory USING (inventory_id)
            JOIN film_category USING (film_id)
            JOIN category USING (category_id)
        WHERE
            quarter = TO_CHAR(payment.payment_date, 'YYYY') || '-Q' || TO_CHAR(payment.payment_date, 'Q')
        GROUP BY
            category.category_id
        HAVING
            SUM(payment.amount) > 0;
END;
$$ LANGUAGE plpgsql;


-- Checking the function with different quarters
SELECT * FROM get_sales_revenue_by_category_qtr('2017-Q1');

SELECT * FROM get_sales_revenue_by_category_qtr('2017-Q2');


/** Task 3. Create a procedure language function called "new_movie" that takes a movie 
*   title as a parameter and inserts a new movie with the given title in the film table. 
*   The function should generate a new unique film ID, set the rental rate to 4.99, 
*   the rental duration to three days, the replacement cost to 19.99, the release year 
*   to the current year, and "language" as Klingon. 
*   The function should also verify that the language exists in the "language" table. 
*   Then, ensure that no such function has been created before; if so, replace it. 
*/
-- Dropping the procedure if it exists
DROP PROCEDURE IF EXISTS new_movie(TEXT);

-- Creating the procedure
CREATE OR REPLACE PROCEDURE new_movie(movie_title TEXT)
LANGUAGE plpgsql AS $$
DECLARE
    new_film_id INTEGER;
    klingon_id INTEGER;
BEGIN
    -- checking if the language exists
    SELECT language_id 
    INTO klingon_id 
    FROM "language"
    WHERE name = 'Klingon';

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language not found: %', 'Klingon';
    END IF;

    -- generating a new unique film ID
    SELECT MAX(film_id) + 1 INTO new_film_id FROM film;

    -- inserting a new movie with the given title and other parameters
    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (new_film_id, movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), klingon_id);

    -- committing the transaction
    COMMIT;
END;$$;


-- Checking the procedure before inserting "Klingon" into the language table
CALL new_movie('New Movie');
SELECT * FROM "film" WHERE "title" = 'New Movie';

-- Inserting "Klingon" into the language table
INSERT INTO "language" ("name") VALUES ('Klingon');

-- Calling the procedure again
CALL new_movie('New Movie');
SELECT * FROM "film" WHERE "title" = 'New Movie';