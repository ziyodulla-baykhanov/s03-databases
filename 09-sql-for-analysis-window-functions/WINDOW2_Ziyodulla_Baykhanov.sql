/**
* Task Description:
* Identify the subcategories of products with consistently higher sales
* from 1998 to 2001 compared to the previous year. 
* Follow the instructions below:
* 1. Determine the sales for each subcategory from 1998 to 2001.
* 2. Calculate the sales for the previous year for each subcategory.
* 3. Identify subcategories where the sales from 1998 to 2001 are consistently higher than the previous year.
* 4. Generate a dataset with a single column containing the identified prod_subcategory values.
*/

SELECT DISTINCT prod_subcategory
FROM (
    -- Inner query to calculate sales and previous year's sales for each product subcategory
    SELECT 
        p.prod_subcategory, 
        t.calendar_year, 
        SUM(s.amount_sold) AS sales,  -- Total sales for the product subcategory for the year
        -- Total sales for the product subcategory for the previous year
        LAG(SUM(s.amount_sold)) OVER (PARTITION BY p.prod_subcategory ORDER BY t.calendar_year) AS prev_sales
    FROM 
        sh.products p 
        JOIN sh.sales s ON p.prod_id = s.prod_id 
        JOIN sh.times t ON s.time_id = t.time_id 
    -- Filter by the years from 1998 to 2001
    WHERE t.calendar_year IN (1998, 1999, 2001)  
    -- Group by product subcategory and calendar year
    GROUP BY p.prod_subcategory, t.calendar_year 
) sub
-- Filter for product subcategories where sales are greater than the previous year's sales
WHERE sales > prev_sales;

/**
* Description of the query:
* This query identifies product subcategories with consistently higher sales from 1998 to 2001 compared to the previous year.
*
* The inner query calculates the total sales for each product subcategory for each year from 1998 to 2001. 
* It does this by joining the products, sales, and times tables on the appropriate keys, 
* filtering for the years 1998 to 2001, and grouping by product subcategory and calendar year. 
*
* The SUM function is used to calculate the total sales for each group.
*
* The LAG function is a window function that returns the value of the previous row in the window. 
* In this case, it is used to calculate the total sales for the previous year for each product subcategory. 
* The PARTITION BY clause specifies that the window is defined by the product subcategory, 
* and the ORDER BY clause specifies that the rows within each window are ordered by calendar year. 
* This ensures that the LAG function returns the total sales for the previous year for each product subcategory.
*
* The outer query selects distinct product subcategories where the total sales for each year 
* from 1998 to 2001 are greater than the total sales for the previous year. 
* This is done using the WHERE clause, which filters for rows where sales > prev_sales. 
*
* The DISTINCT keyword ensures that each product subcategory is only included once in the result.
*
* This query demonstrates the use of window functions, aggregation functions, 
* and joins in SQL to perform complex calculations and generate reports based on specific conditions. 
* The result is a list of product subcategories that have consistently higher sales 
* from 1998 to 2001 compared to the previous year. This information could be useful 
* for identifying successful product subcategories, analyzing sales trends, and making business decisions.
*/
