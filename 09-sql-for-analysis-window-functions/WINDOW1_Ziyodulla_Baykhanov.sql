/**
* Task Description:
* Construct a query to generate a sales report for customers ranked 
* in the top 300 based on total sales in the years 1998, 1999, and 2001. 
* The report should be categorized based on sales channels, 
* and separate calculations should be performed for each channel. 
* Consider the following instructions:
* 1. Retrieve customers who ranked among the top 300 in sales for the years 1998, 1999, and 2001.
* 2. Categorize the customers based on their sales channels.
* 3. Perform separate calculations for each sales channel.
* 4. Include in the report only purchases made on the channel specified.
* 5. Format the column so that total sales are displayed with two decimal places.
*/

-- Create a CTE to calculate total sales for each customer, for each channel, for each year
WITH sales_by_channel AS (
    SELECT 
        c.cust_id, 
        c.cust_first_name, 
        c.cust_last_name, 
        ch.channel_desc, 
        t.calendar_year, 
        SUM(s.amount_sold) AS total_sales,
        -- Rank customers within each channel for each year based on their total sales
        ROW_NUMBER() OVER (PARTITION BY ch.channel_id, t.calendar_year ORDER BY SUM(s.amount_sold) DESC) AS channel_rank
    FROM sh.customers c
        JOIN sh.sales s ON c.cust_id = s.cust_id
        JOIN sh.channels ch ON s.channel_id = ch.channel_id
        JOIN sh.times t ON s.time_id = t.time_id
    WHERE 
        t.calendar_year IN (1998, 1999, 2001)
    GROUP BY 
        c.cust_id, c.cust_first_name, c.cust_last_name, ch.channel_id, t.calendar_year
    HAVING SUM(s.amount_sold) > 0
),
-- Create another CTE to rank these customers based on their total sales across all channels for each year
top_sales_by_year AS (
    SELECT 
        cust_id, 
        cust_first_name, 
        cust_last_name, 
        channel_desc, 
        calendar_year,
        total_sales, 
        -- Rank customers for each year based on their total sales across all channels
        ROW_NUMBER() OVER (PARTITION BY calendar_year ORDER BY total_sales DESC) AS yearly_rank
    FROM sales_by_channel
    WHERE channel_rank <= 300
)
-- Select the top 300 customers for each year based on their total sales across all channels
SELECT 
    t.cust_id AS customer_id, 
    t.cust_first_name AS customer_first_name, 
    t.cust_last_name AS customer_last_name, 
    t.channel_desc AS sales_channel, 
    t.calendar_year AS sales_year,
    -- Round total sales to 2 decimal places
    ROUND(t.total_sales, 2) AS total_sales, 
    t.yearly_rank AS rank_by_year
FROM top_sales_by_year t
WHERE t.yearly_rank <= 300
ORDER BY t.calendar_year, t.yearly_rank;

/**
* Description of the query:
* This query is designed to generate a sales report for customers who are ranked 
* in the top 300 based on total sales in the years 1998, 1999, and 2001. 
* The report is categorized based on sales channels, and separate calculations are performed for each channel.
*
* - sales_by_channel CTE: This CTE calculates the total sales for each customer, for each channel, for each year. 
* It then ranks the customers within each channel for each year based on their total sales.
*
* - top_sales_by_year CTE: This CTE takes the output of the first CTE and ranks these customers 
* based on their total sales across all channels for each year.
* 
* - Final SELECT statement: This statement selects the top 300 customers for each year based on their 
* total sales across all channels. It also rounds the total sales to 2 decimal places for better readability.
*
* The query uses window functions ROW_NUMBER() to rank the customers. 
* The PARTITION BY clause in the window function ensures that the ranking is done separately 
* for each group (channel or year). The ORDER BY clause in the window function specifies 
* the ordering of the data within each partition (based on total sales). 
* The HAVING clause in the first CTE ensures that only customers with non-zero sales are included. 
* The WHERE clauses in the second CTE and the final SELECT statement filter out customers 
* who are not in the top 300. The ROUND() function in the final SELECT statement 
* rounds the total sales to 2 decimal places. The final ORDER BY clause orders the output by year and rank.
*
* This query demonstrates the power of SQL window functions in generating complex reports 
* that require ranking and partitioning of data. It also shows how CTEs can be used to organize 
* the query into logical steps, making the query easier to understand and maintain.
*/