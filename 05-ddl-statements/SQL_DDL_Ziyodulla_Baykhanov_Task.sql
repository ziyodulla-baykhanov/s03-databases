-- Homework on the topic "DDL Statements". Creating a Physical Database.

-- Solution to the tasks No. 1, 2, 3 and 4:
-- (1) Create a physical database with a separate database and 
--     schema and give it an appropriate domain-related name.
-- (2) Create tables based on the 3nf model developed during the DB Basics module.
-- (3) Use appropriate data types for each column and apply NOT NULL constraints, 
--     DEFAULT values, and GENERATED ALWAYS AS columns as required.
-- (4) Create relationships between tables using primary and foreign keys.


-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 1.1.0-alpha
-- PostgreSQL version: 15.0
-- Project Site: pgmodeler.io
-- Model Author: Ziyodulla Baykhanov

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- object: international_school | type: DATABASE --
-- DROP DATABASE IF EXISTS international_school;
CREATE DATABASE international_school;
-- ddl-end --


-- object: public.pupils | type: TABLE --
-- DROP TABLE IF EXISTS public.pupils CASCADE;
CREATE TABLE public.pupils (
	p_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	p_first_name varchar(150) NOT NULL,
	p_last_name varchar(150) NOT NULL,
	p_date_of_birth date NOT NULL,
	p_age integer,
	p_enrollment_year integer NOT NULL DEFAULT EXTRACT(year FROM current_date),
	p_email varchar(150) NOT NULL,
	c_id_citizenship smallint NOT NULL,
	g_id_gender smallint NOT NULL,
	pt_id_payment_types smallint NOT NULL,
	sg_id_study_group integer,
	CONSTRAINT pupils_pk PRIMARY KEY (p_id)
);
-- ddl-end --
COMMENT ON TABLE public.pupils IS E'Details of individuals who study at the school.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_id IS E'Unique identifier for each pupil in the school.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_first_name IS E'First name of the pupil.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_last_name IS E'Last name of the pupil.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_date_of_birth IS E'Date of birth of the pupil.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_age IS E'Contains information about the age of the pupil.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_enrollment_year IS E'Contains information about which year the pupil was enrolled to the school.';
-- ddl-end --
COMMENT ON COLUMN public.pupils.p_email IS E'Email address of the pupil.';
-- ddl-end --
COMMENT ON CONSTRAINT pupils_pk ON public.pupils IS E'Primary key constraint to pupils ID.';
-- ddl-end --
ALTER TABLE public.pupils OWNER TO postgres;
-- ddl-end --

-- object: public.payment_types | type: TABLE --
-- DROP TABLE IF EXISTS public.payment_types CASCADE;
CREATE TABLE public.payment_types (
	pt_id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 32767 START WITH 1 CACHE 1 ),
	pt_name varchar(150) NOT NULL,
	pt_description varchar(3000),
	CONSTRAINT payment_types_pk PRIMARY KEY (pt_id),
	CONSTRAINT "UNQ_payment_type_name" UNIQUE (pt_name)
);
-- ddl-end --
COMMENT ON TABLE public.payment_types IS E'Describes the types of payment for pupipls (e.g. contract, sponsored, etc.)';
-- ddl-end --
COMMENT ON COLUMN public.payment_types.pt_id IS E'Unique identifier for each payment type.';
-- ddl-end --
COMMENT ON COLUMN public.payment_types.pt_name IS E'Name of the payment type.';
-- ddl-end --
COMMENT ON COLUMN public.payment_types.pt_description IS E'Describes each payment type.';
-- ddl-end --
COMMENT ON CONSTRAINT payment_types_pk ON public.payment_types IS E'Primary key constraint to payment type ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_payment_type_name" ON public.payment_types IS E'Constraint to ensure that the payment type name contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.payment_types OWNER TO postgres;
-- ddl-end --

-- object: public.subjects | type: TABLE --
-- DROP TABLE IF EXISTS public.subjects CASCADE;
CREATE TABLE public.subjects (
	sb_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	sb_name varchar(150) NOT NULL,
	sb_description varchar(3000),
	CONSTRAINT subjects_pk PRIMARY KEY (sb_id),
	CONSTRAINT "UNQ_subject_name" UNIQUE (sb_name)
);
-- ddl-end --
COMMENT ON TABLE public.subjects IS E'Information about subjects taught in the school.';
-- ddl-end --
COMMENT ON COLUMN public.subjects.sb_id IS E'Unique identifier for each subject.';
-- ddl-end --
COMMENT ON COLUMN public.subjects.sb_name IS E'Name of the subject.';
-- ddl-end --
COMMENT ON COLUMN public.subjects.sb_description IS E'Describes the matter of the subject.';
-- ddl-end --
COMMENT ON CONSTRAINT subjects_pk ON public.subjects IS E'Primary key constraint to subject ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_subject_name" ON public.subjects IS E'Constraint to ensure that the subject name contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.subjects OWNER TO postgres;
-- ddl-end --

-- object: payment_types_fk | type: CONSTRAINT --
-- ALTER TABLE public.pupils DROP CONSTRAINT IF EXISTS payment_types_fk CASCADE;
ALTER TABLE public.pupils ADD CONSTRAINT payment_types_fk FOREIGN KEY (pt_id_payment_types)
REFERENCES public.payment_types (pt_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.m2m_subjects_to_pupils | type: TABLE --
-- DROP TABLE IF EXISTS public.m2m_subjects_to_pupils CASCADE;
CREATE TABLE public.m2m_subjects_to_pupils (
	sb_id_subjects integer NOT NULL,
	p_id_pupils integer NOT NULL,
	CONSTRAINT m2m_subjects_to_pupils_pk PRIMARY KEY (sb_id_subjects,p_id_pupils)
);
-- ddl-end --
COMMENT ON TABLE public.m2m_subjects_to_pupils IS E'Association table linking pupils and their subjects.';
-- ddl-end --

-- object: subjects_fk | type: CONSTRAINT --
-- ALTER TABLE public.m2m_subjects_to_pupils DROP CONSTRAINT IF EXISTS subjects_fk CASCADE;
ALTER TABLE public.m2m_subjects_to_pupils ADD CONSTRAINT subjects_fk FOREIGN KEY (sb_id_subjects)
REFERENCES public.subjects (sb_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: pupils_fk | type: CONSTRAINT --
-- ALTER TABLE public.m2m_subjects_to_pupils DROP CONSTRAINT IF EXISTS pupils_fk CASCADE;
ALTER TABLE public.m2m_subjects_to_pupils ADD CONSTRAINT pupils_fk FOREIGN KEY (p_id_pupils)
REFERENCES public.pupils (p_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.educational_stages | type: TABLE --
-- DROP TABLE IF EXISTS public.educational_stages CASCADE;
CREATE TABLE public.educational_stages (
	es_id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 32767 START WITH 1 CACHE 1 ),
	es_name varchar(128) NOT NULL,
	es_description varchar(1000),
	CONSTRAINT educational_stages_pk PRIMARY KEY (es_id),
	CONSTRAINT "UNQ_es_name" UNIQUE (es_name)
);
-- ddl-end --
COMMENT ON TABLE public.educational_stages IS E'Describes the educational stages available in the school (e.g. Preschool, Elementary school, Middle school, High school).';
-- ddl-end --
COMMENT ON COLUMN public.educational_stages.es_id IS E'Unique identifier for each educational stage.';
-- ddl-end --
COMMENT ON COLUMN public.educational_stages.es_name IS E'Name of the educational stage.';
-- ddl-end --
COMMENT ON COLUMN public.educational_stages.es_description IS E'Description of each educational stage.';
-- ddl-end --
COMMENT ON CONSTRAINT educational_stages_pk ON public.educational_stages IS E'Primary key constraint to educational stage ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_es_name" ON public.educational_stages IS E'Constraint to ensure that the educational stage name contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.educational_stages OWNER TO postgres;
-- ddl-end --

-- object: public.teachers | type: TABLE --
-- DROP TABLE IF EXISTS public.teachers CASCADE;
CREATE TABLE public.teachers (
	t_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	t_first_name varchar(150) NOT NULL,
	t_last_name varchar(150) NOT NULL,
	t_date_of_birth date NOT NULL,
	t_age integer,
	t_email varchar(150) NOT NULL,
	c_id_citizenship smallint NOT NULL,
	g_id_gender smallint NOT NULL,
	et_id_employment_types smallint NOT NULL,
	CONSTRAINT teachers_pk PRIMARY KEY (t_id)
);
-- ddl-end --
COMMENT ON TABLE public.teachers IS E'Details of individuals who teaches pupil.';
-- ddl-end --
COMMENT ON COLUMN public.teachers.t_id IS E'Unique identifier for each teacher.';
-- ddl-end --
COMMENT ON COLUMN public.teachers.t_first_name IS E'First name of the teacher.';
-- ddl-end --
COMMENT ON COLUMN public.teachers.t_last_name IS E'Last name of the teacher.';
-- ddl-end --
COMMENT ON COLUMN public.teachers.t_date_of_birth IS E'Date of birth of the teacher.';
-- ddl-end --
COMMENT ON COLUMN public.teachers.t_age IS E'Contains information about the age of the teacher.';
-- ddl-end --
COMMENT ON COLUMN public.teachers.t_email IS E'Email address of the teacher.';
-- ddl-end --
COMMENT ON CONSTRAINT teachers_pk ON public.teachers IS E'Primary key constraint to teacher ID.';
-- ddl-end --
ALTER TABLE public.teachers OWNER TO postgres;
-- ddl-end --

-- object: public.employment_types | type: TABLE --
-- DROP TABLE IF EXISTS public.employment_types CASCADE;
CREATE TABLE public.employment_types (
	et_id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 32767 START WITH 1 CACHE 1 ),
	et_name varchar(128) NOT NULL,
	et_description varchar(1000),
	CONSTRAINT employment_types_pk PRIMARY KEY (et_id),
	CONSTRAINT "UNQ_et" UNIQUE (et_name)
);
-- ddl-end --
COMMENT ON TABLE public.employment_types IS E'Describes the employment types of school staff (e.g. Full-time, Part-time, Contract).';
-- ddl-end --
COMMENT ON COLUMN public.employment_types.et_id IS E'Unique identifier for each employment type.';
-- ddl-end --
COMMENT ON COLUMN public.employment_types.et_name IS E'Name of the employment type.';
-- ddl-end --
COMMENT ON COLUMN public.employment_types.et_description IS E'Describes each employment type.';
-- ddl-end --
COMMENT ON CONSTRAINT employment_types_pk ON public.employment_types IS E'Primary key constraint to employment type ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_et" ON public.employment_types IS E'Constraint to ensure that the employment type contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.employment_types OWNER TO postgres;
-- ddl-end --

-- object: public.study_group | type: TABLE --
-- DROP TABLE IF EXISTS public.study_group CASCADE;
CREATE TABLE public.study_group (
	sg_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	sg_name varchar(128) NOT NULL,
	sg_description varchar(1000),
	es_id_educational_stages smallint NOT NULL,
	CONSTRAINT study_group_pk PRIMARY KEY (sg_id),
	CONSTRAINT "UNQ_sg_name" UNIQUE (sg_name)
);
-- ddl-end --
COMMENT ON TABLE public.study_group IS E'Describes the study group (classess), which pupils attend within a specific course or lesson at the school.';
-- ddl-end --
COMMENT ON COLUMN public.study_group.sg_id IS E'Unique identifier for each study group.';
-- ddl-end --
COMMENT ON COLUMN public.study_group.sg_name IS E'Name of the stydy group.';
-- ddl-end --
COMMENT ON COLUMN public.study_group.sg_description IS E'Decribes each study group.';
-- ddl-end --
COMMENT ON CONSTRAINT study_group_pk ON public.study_group IS E'Primary key constraint to study group ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_sg_name" ON public.study_group IS E'Constraint to ensure that the study group name contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.study_group OWNER TO postgres;
-- ddl-end --

-- object: public.citizenship | type: TABLE --
-- DROP TABLE IF EXISTS public.citizenship CASCADE;
CREATE TABLE public.citizenship (
	c_id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 32767 START WITH 1 CACHE 1 ),
	cntr_id_country smallint NOT NULL,
	CONSTRAINT citizenship_pk PRIMARY KEY (c_id)
);
-- ddl-end --
COMMENT ON TABLE public.citizenship IS E'Describes the citizenship of individuals who are members of the international school.';
-- ddl-end --
COMMENT ON COLUMN public.citizenship.c_id IS E'Unique identifier for each citizenship.';
-- ddl-end --
COMMENT ON CONSTRAINT citizenship_pk ON public.citizenship IS E'Primary key constraint to citizenship ID.';
-- ddl-end --
ALTER TABLE public.citizenship OWNER TO postgres;
-- ddl-end --

-- object: public.country | type: TABLE --
-- DROP TABLE IF EXISTS public.country CASCADE;
CREATE TABLE public.country (
	cntr_id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 32767 START WITH 1 CACHE 1 ),
	cntr_name varchar(250) NOT NULL,
	CONSTRAINT country_pk PRIMARY KEY (cntr_id),
	CONSTRAINT "UNQ_cntr_name" UNIQUE (cntr_name)
);
-- ddl-end --
COMMENT ON TABLE public.country IS E'Contains information about countries in the world.';
-- ddl-end --
COMMENT ON COLUMN public.country.cntr_id IS E'Unique identifier for each country.';
-- ddl-end --
COMMENT ON COLUMN public.country.cntr_name IS E'Official full name of the country in English.';
-- ddl-end --
COMMENT ON CONSTRAINT country_pk ON public.country IS E'Primary key constraint to country ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_cntr_name" ON public.country IS E'Constraint to ensure that the country name contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.country OWNER TO postgres;
-- ddl-end --

-- object: public.gender | type: TABLE --
-- DROP TABLE IF EXISTS public.gender CASCADE;
CREATE TABLE public.gender (
	g_id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 32767 START WITH 1 CACHE 1 ),
	g_name varchar(20) NOT NULL,
	CONSTRAINT gender_types_pk PRIMARY KEY (g_id),
	CONSTRAINT "UNQ_gt_name" UNIQUE (g_name)
);
-- ddl-end --
COMMENT ON TABLE public.gender IS E'Contains information about gender types.';
-- ddl-end --
COMMENT ON COLUMN public.gender.g_id IS E'Unique identifier for each gender type.';
-- ddl-end --
COMMENT ON COLUMN public.gender.g_name IS E'Name of each gender.';
-- ddl-end --
COMMENT ON CONSTRAINT gender_types_pk ON public.gender IS E'Primary key constraint to gender type ID.';
-- ddl-end --
COMMENT ON CONSTRAINT "UNQ_gt_name" ON public.gender IS E'Constraint to ensure that the gender type name contained in a column is unique among all the rows in the table.';
-- ddl-end --
ALTER TABLE public.gender OWNER TO postgres;
-- ddl-end --

-- object: country_fk | type: CONSTRAINT --
-- ALTER TABLE public.citizenship DROP CONSTRAINT IF EXISTS country_fk CASCADE;
ALTER TABLE public.citizenship ADD CONSTRAINT country_fk FOREIGN KEY (cntr_id_country)
REFERENCES public.country (cntr_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: citizenship_uq | type: CONSTRAINT --
-- ALTER TABLE public.citizenship DROP CONSTRAINT IF EXISTS citizenship_uq CASCADE;
ALTER TABLE public.citizenship ADD CONSTRAINT citizenship_uq UNIQUE (cntr_id_country);
-- ddl-end --

-- object: citizenship_fk | type: CONSTRAINT --
-- ALTER TABLE public.pupils DROP CONSTRAINT IF EXISTS citizenship_fk CASCADE;
ALTER TABLE public.pupils ADD CONSTRAINT citizenship_fk FOREIGN KEY (c_id_citizenship)
REFERENCES public.citizenship (c_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: citizenship_fk | type: CONSTRAINT --
-- ALTER TABLE public.teachers DROP CONSTRAINT IF EXISTS citizenship_fk CASCADE;
ALTER TABLE public.teachers ADD CONSTRAINT citizenship_fk FOREIGN KEY (c_id_citizenship)
REFERENCES public.citizenship (c_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: gender_fk | type: CONSTRAINT --
-- ALTER TABLE public.pupils DROP CONSTRAINT IF EXISTS gender_fk CASCADE;
ALTER TABLE public.pupils ADD CONSTRAINT gender_fk FOREIGN KEY (g_id_gender)
REFERENCES public.gender (g_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: gender_fk | type: CONSTRAINT --
-- ALTER TABLE public.teachers DROP CONSTRAINT IF EXISTS gender_fk CASCADE;
ALTER TABLE public.teachers ADD CONSTRAINT gender_fk FOREIGN KEY (g_id_gender)
REFERENCES public.gender (g_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: employment_types_fk | type: CONSTRAINT --
-- ALTER TABLE public.teachers DROP CONSTRAINT IF EXISTS employment_types_fk CASCADE;
ALTER TABLE public.teachers ADD CONSTRAINT employment_types_fk FOREIGN KEY (et_id_employment_types)
REFERENCES public.employment_types (et_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: study_group_fk | type: CONSTRAINT --
-- ALTER TABLE public.pupils DROP CONSTRAINT IF EXISTS study_group_fk CASCADE;
ALTER TABLE public.pupils ADD CONSTRAINT study_group_fk FOREIGN KEY (sg_id_study_group)
REFERENCES public.study_group (sg_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.m2m_subjects_to_teachers | type: TABLE --
-- DROP TABLE IF EXISTS public.m2m_subjects_to_teachers CASCADE;
CREATE TABLE public.m2m_subjects_to_teachers (
	sb_id_subjects integer NOT NULL,
	t_id_teachers integer NOT NULL,
	CONSTRAINT m2m_subjects_to_teachers_pk PRIMARY KEY (sb_id_subjects,t_id_teachers)
);
-- ddl-end --

-- object: subjects_fk | type: CONSTRAINT --
-- ALTER TABLE public.m2m_subjects_to_teachers DROP CONSTRAINT IF EXISTS subjects_fk CASCADE;
ALTER TABLE public.m2m_subjects_to_teachers ADD CONSTRAINT subjects_fk FOREIGN KEY (sb_id_subjects)
REFERENCES public.subjects (sb_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: teachers_fk | type: CONSTRAINT --
-- ALTER TABLE public.m2m_subjects_to_teachers DROP CONSTRAINT IF EXISTS teachers_fk CASCADE;
ALTER TABLE public.m2m_subjects_to_teachers ADD CONSTRAINT teachers_fk FOREIGN KEY (t_id_teachers)
REFERENCES public.teachers (t_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: public.m2m_study_group_to_teachers | type: TABLE --
-- DROP TABLE IF EXISTS public.m2m_study_group_to_teachers CASCADE;
CREATE TABLE public.m2m_study_group_to_teachers (
	sg_id_study_group integer NOT NULL,
	t_id_teachers integer NOT NULL,
	CONSTRAINT m2m_study_group_to_teachers_pk PRIMARY KEY (sg_id_study_group,t_id_teachers)
);
-- ddl-end --

-- object: study_group_fk | type: CONSTRAINT --
-- ALTER TABLE public.m2m_study_group_to_teachers DROP CONSTRAINT IF EXISTS study_group_fk CASCADE;
ALTER TABLE public.m2m_study_group_to_teachers ADD CONSTRAINT study_group_fk FOREIGN KEY (sg_id_study_group)
REFERENCES public.study_group (sg_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: teachers_fk | type: CONSTRAINT --
-- ALTER TABLE public.m2m_study_group_to_teachers DROP CONSTRAINT IF EXISTS teachers_fk CASCADE;
ALTER TABLE public.m2m_study_group_to_teachers ADD CONSTRAINT teachers_fk FOREIGN KEY (t_id_teachers)
REFERENCES public.teachers (t_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: educational_stages_fk | type: CONSTRAINT --
-- ALTER TABLE public.study_group DROP CONSTRAINT IF EXISTS educational_stages_fk CASCADE;
ALTER TABLE public.study_group ADD CONSTRAINT educational_stages_fk FOREIGN KEY (es_id_educational_stages)
REFERENCES public.educational_stages (es_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --


-- *************************************
-- Solution to the task No.5: 
-- Apply five check constraints across the tables to restrict certain values, including
-- (1) date to be inserted, which must be greater than January 1, 2000

ALTER TABLE pupils 
ADD CHECK (p_date_of_birth > '2000-01-01'::DATE);

ALTER TABLE pupils 
ADD CHECK (p_enrollment_year::NUMERIC > EXTRACT(YEAR FROM p_date_of_birth));


-- (2) inserted measured value that cannot be negative

ALTER TABLE pupils 
ADD CHECK (p_age > 0);

ALTER TABLE teachers 
ADD CHECK (t_age > 0);


-- (3) inserted value that can only be a specific value (as an example of gender)

ALTER TABLE gender 
ADD CHECK (g_name IN ('Male', 'Female'));


ALTER TABLE educational_stages 
ADD CHECK (es_name IN ('Preschool', 'Elementary school', 'Middle school', 'High school'));


-- (4) unique

ALTER TABLE pupils
ADD UNIQUE (p_email);

ALTER TABLE teachers
ADD UNIQUE (t_email);


-- (5) not null

ALTER TABLE pupils 
ALTER COLUMN p_age 
SET NOT NULL;

ALTER TABLE teachers 
ALTER COLUMN t_age 
SET NOT NULL;


-- *************************************
-- Solution the to task No.6:
-- Populate the tables with the sample data generated, ensuring each table
-- has at least two rows (for a total of 20+ rows in all the tables).

-- (1) Inserting values to 'country' table

INSERT INTO public.country
	(cntr_name)
VALUES 
	('Republic of Uzbekistan'),
	('United Arab Emirates'),
	('United Kingdom of Great Britain and Northern Ireland'),
	('United States of America');

-- (2) Inserting values to 'citizenship' table

INSERT INTO public.citizenship
	(cntr_id_country)
VALUES 	(1), (2), (3), (4);

-- (3) Inserting values to 'gender' table

INSERT INTO public.gender
	(g_name)
VALUES	('Male'), ('Female');

-- (4) Inserting values to 'educational_stages' table

INSERT INTO public.educational_stages
	(es_name, es_description)
VALUES	
	('Preschool', 'Educational stage for children who are in the age between 5-8'),
	('Elementary school', 'Educational stage for children who are in the age between 6-13'),
	('Middle school', 'Educational stage for children who are in the age between 11-19'),
	('High school', 'Educational stage for children who are in the age between 17-20');

-- (5) Inserting values to 'payment_types' table

INSERT INTO public.payment_types
	(pt_name, pt_description)
VALUES	
	('Commercial contract', 'A client or private company/organization pays the tuition fees for a pupil.'),
	('Governmental quote', 'A government agency or department pays the tuition fees for a pupil.'),
	('Sponsorship', 'A third party, such as a charity, foundation, or non-governmental organization, pays the tuition fees for a pupil based on certain criteria.'),
	('Scholarship', 'The school itself pays the tuition fees for a pupil based on their academic excellence, talent, or potential.');

-- (6) Inserting values to 'employment_types' table

INSERT INTO public.employment_types
	(et_name, et_description)
VALUES	
	('Full-time', 'A teacher who works for the whole year and gets a salary and benefits.'),
	('Part-time', 'A teacher who works for part of the year or week and gets a proportional salary and benefits.'),
	('Contract', 'A teacher who works for a specific time or project and gets a fixed payment and benefits.');

-- (7) Inserting values to 'subjects' table

INSERT INTO public.subjects
	(sb_name, sb_description)
VALUES	
	('Mathematics A1', 'A subject that introduces students to the basic concepts and skills of numbers, operations, patterns, shapes, and data analysis.'),
	('Mathematics E3', 'A subject that develops students’ understanding and application of algebra, geometry, trigonometry, statistics, and calculus.'),
	('English A1', 'A subject that helps students learn the fundamentals of the English language, such as phonics, vocabulary, grammar, and spelling.'),
	('English E3', 'A subject that enhances students’ proficiency and appreciation of the English language and literature, such as reading comprehension, writing styles, genres, and texts.'),
	('Science A1', 'A subject that exposes students to the wonders and diversity of the natural world, such as plants, animals, weather, seasons, and materials.'),
	('Science E3', 'A subject that explores the principles and phenomena of the natural sciences, such as physics, chemistry, biology, and earth science.'),
	('History A1', 'A subject that familiarizes students with the stories and events of the past, such as ancient civilizations, explorers, inventors, and leaders.'),
	('History E3', 'A subject that examines the causes and effects of the historical events and movements that shaped the world, such as world wars, revolutions, conflicts, and human rights.'),
	('Art A1', 'A subject that encourages students to express their creativity and imagination through various forms of art, such as drawing, painting, collage, and clay.'),
	('Art E3', 'A subject that challenges students to develop their artistic skills and techniques through various media and styles of art, such as sculpture, photography, music, and drama.');


-- (8) Inserting values to 'study_group' table

INSERT INTO public.study_group
	(sg_name, sg_description, es_id_educational_stages)
VALUES	
	('EL-G1', 'Elementary school stage group No. 1', '2'),
	('EL-G2', 'Elementary school stage group No. 2', '2'),
	('MD-G2', 'Middle school stage group No. 2', '3'),
	('MD-G3', 'Middle school stage group No. 3', '3');


-- (9) Inserting values to 'pupils' table

INSERT INTO public.pupils
	(p_first_name, p_last_name, p_date_of_birth, p_age, p_enrollment_year, p_email, c_id_citizenship, g_id_gender, pt_id_payment_types, sg_id_study_group)
VALUES	
	('Aisha', 'Ahmed', '2016-01-11', '7', '2023', 'aisha.ahmed@student.ischool.edu', '2', '2', '1', '1'),
	('Carlos', 'Gomez', '2016-06-12', '7', '2023', 'carlos.gomez@student.ischool.edu', '4', '1', '2', '2'),
	('Emma', 'Lee', '2011-02-02', '12', '2018', 'emma.lee@student.ischool.edu', '1', '2', '3', '3'),
	('Hiroshi', 'Nakamura', '2011-03-10', '12', '2018', 'hiroshi.nakamura@student.ischool.edu', '3', '1', '1', '4'),
	('Julia', 'Schmidt', '2016-05-04', '7', '2023', 'julia.schmidt@student.ischool.edu', '2', '2', '2', '1'),
	('Kevin', 'Chen', '2016-07-11', '7', '2023', 'kevin.chen@student.ischool.edu', '3', '1', '1', '2'),
	('Nadia', 'Hassan', '2011-08-03', '12', '2019', 'nadia.hassan@student.ischool.edu', '2', '2', '2', '3'),
	('Rajesh', 'Patel', '2011-11-10', '12', '2020', 'rajesh.patel@student.ischool.edu', '3', '1', '3', '4');


-- (10) Inserting values to 'teachers' table

INSERT INTO public.teachers
	(t_first_name, t_last_name, t_date_of_birth, t_age, t_email, c_id_citizenship, g_id_gender, et_id_employment_types)
VALUES	
	('David', 'Smith', '1993-02-12', '30', 'david.smith@ischool.edu', '4', '1', '1'),
	('Maria', 'Garcia', '1994-07-08', '29', 'maria.garcia@ischool.edu', '1', '2', '2'),
	('Chen', 'Li', '1994-05-01', '28', 'chen.li@ischool.edu', '3', '2', '3'),
	('Fatima', 'Khan', '1992-09-09', '31', 'fatima.khan@ischool.edu', '2', '2', '1'),
	('Jean', 'Dupont', '1990-11-07', '33', 'jean.dupont@ischool.edu', '4', '1', '2');

-- (11) Inserting values to 'm2m_study_group_to_teachers' table

INSERT INTO public.m2m_study_group_to_teachers
	(sg_id_study_group, t_id_teachers)
VALUES	
	('1', '1'),
	('2', '2'),
	('3', '1'),
	('4', '2'),
	('1', '3'),
	('2', '4'),
	('3', '3'),
	('4', '4'),
	('1', '5'),
	('2', '5'),
	('3', '5'),
	('4', '5');

-- (12) Inserting values to 'm2m_subjects_to_teachers' table

INSERT INTO public.m2m_subjects_to_teachers
	(sb_id_subjects, t_id_teachers)
VALUES	
	('1', '1'),
	('2', '2'),
	('3', '3'),
	('4', '4'),
	('5', '5');

-- (13) Inserting values to 'm2m_subjects_to_pupils' table

INSERT INTO public.m2m_subjects_to_pupils
	(sb_id_subjects, p_id_pupils)
VALUES	
	('1', '1'),
	('2', '3'),
	('3', '1'),
	('4', '3'),
	('5', '1'),
	('6', '3'),
	('7', '1'),
	('8', '3'),
	('9', '1'),
	('10', '3'),
	('1', '2'),
	('2', '4'),
	('3', '2'),
	('4', '4'),
	('5', '2'),
	('6', '4'),
	('7', '2'),
	('8', '4'),
	('9', '2'),
	('10', '4');


-- *************************************
-- Solution to the task No. 7.
-- (1) Add a 'record_ts' field to each table using ALTER TABLE statements, set the default value to current_date

ALTER TABLE public.country
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.citizenship
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.gender
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.educational_stages
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.payment_types
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.employment_types
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.subjects
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.study_group
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.pupils
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.teachers
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.m2m_study_group_to_teachers
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.m2m_subjects_to_teachers
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;

ALTER TABLE public.m2m_subjects_to_pupils
ADD COLUMN "record_ts" DATE,
ALTER COLUMN "record_ts" SET DEFAULT CURRENT_DATE;


-- (2) and check to make sure the value has been set for the existing rows.

UPDATE public.country
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.citizenship
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.gender
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.educational_stages
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.payment_types
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.employment_types
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.subjects
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.study_group
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.pupils
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.teachers
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.m2m_study_group_to_teachers
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.m2m_subjects_to_teachers
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

UPDATE public.m2m_subjects_to_pupils
SET record_ts = CURRENT_DATE
WHERE record_ts IS NULL;

-- View all the tables

SELECT * FROM public.country;

SELECT * FROM public.citizenship;

SELECT * FROM public.gender;

SELECT * FROM public.educational_stages;

SELECT * FROM public.payment_types;

SELECT * FROM public.employment_types;

SELECT * FROM public.subjects;

SELECT * FROM public.study_group;

SELECT * FROM public.pupils;

SELECT * FROM public.teachers;

SELECT * FROM public.m2m_study_group_to_teachers;

SELECT * FROM public.m2m_subjects_to_teachers;

SELECT * FROM public.m2m_subjects_to_pupils;


-- *************************************
-- End of the howework