/* Task 1
 * Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
 */
-- Query 1
SELECT store_id, first_name, last_name, total_revenue
FROM (
    SELECT 
        s.store_id, 
        st.first_name, 
        st.last_name, 
        SUM(p.amount) AS total_revenue,
        ROW_NUMBER() OVER(PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) as rn
    FROM payment p
    JOIN staff st ON p.staff_id = st.staff_id
    JOIN store s ON st.store_id = s.store_id
    WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY s.store_id, st.staff_id, st.first_name, st.last_name
) t
WHERE rn = 1;


-- Query 2
WITH staff_revenue AS (
    SELECT 
        s.store_id, 
        st.staff_id, 
        st.first_name, 
        st.last_name, 
        SUM(p.amount) AS total_revenue
    FROM payment p
    JOIN staff st ON p.staff_id = st.staff_id
    JOIN store s ON st.store_id = s.store_id
    WHERE EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY s.store_id, st.staff_id
),
max_revenue AS (
    SELECT 
        store_id, 
        MAX(total_revenue) AS max_revenue
    FROM staff_revenue
    GROUP BY store_id
)
SELECT 
    sr.store_id, 
    sr.first_name, 
    sr.last_name, 
    sr.total_revenue
FROM staff_revenue sr
JOIN max_revenue mr ON sr.store_id = mr.store_id AND sr.total_revenue = mr.max_revenue;


/* Task 2
 * Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
 */
-- Query 1
SELECT film.title, COUNT(rental.rental_id) AS rental_count
FROM film
JOIN inventory ON film.film_id = inventory.film_id
JOIN rental ON inventory.inventory_id = rental.inventory_id
GROUP BY film.title
ORDER BY rental_count DESC
LIMIT 5;

-- Query 2
SELECT f.title, COUNT(*) as rental_count
FROM rental r
JOIN inventory i ON r.inventory_id = i.inventory_id
JOIN film f ON i.film_id = f.film_id
GROUP BY f.title
ORDER BY rental_count DESC
LIMIT 5;


/* Task 3
 * Which actors/actresses didn't act for a longer period of time than the others?
 */
-- Query 1
SELECT actor_id, first_name, last_name, MAX(release_year) as last_movie_year
FROM actor
JOIN film_actor USING (actor_id)
JOIN film USING (film_id)
GROUP BY actor_id
ORDER BY last_movie_year ASC;

-- Query 2
SELECT a.actor_id, a.first_name, a.last_name, MAX(f.release_year) as last_movie_year
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON fa.film_id = f.film_id
GROUP BY a.actor_id, a.first_name, a.last_name
ORDER BY last_movie_year ASC;


