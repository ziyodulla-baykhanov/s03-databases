-- Task No. 2. Applying the UPDATE Statement

-- (1) Update the "film" table
-- The film_id of 'The Hobbit: An Unexpected Journey' is 1001
UPDATE public.film
SET rental_duration = 21, rental_rate = 9.99
WHERE film_id = 1001;


-- (2.1) Find an existing customer with at least 10 rental and 10 payment records
SELECT c.customer_id, c.first_name, c.last_name, c.email, c.address_id, COUNT(DISTINCT r.rental_id) AS rental_count, COUNT(DISTINCT p.payment_id) AS payment_count
FROM customer c
JOIN rental r ON c.customer_id = r.customer_id
JOIN payment p ON c.customer_id = p.customer_id
GROUP BY c.customer_id, c.first_name, c.last_name, c.email, c.address_id
HAVING COUNT(DISTINCT r.rental_id) >= 10 AND COUNT(DISTINCT p.payment_id) >= 10
LIMIT 1;


-- (2.2) Change personal data of chosen customer to mine (first name, last name, email, address)
-- We can use customer_id from the output of the previous query (i.e. customer_id = 1)
UPDATE customer
SET first_name = 'ZIYODULLA', last_name = 'BAYKHANOV', email = 'ZIYODULLA.BAYKHANOV@sakilacustomer.org', address_id = 89
WHERE customer_id = 1;


-- (3) Update the customer's create_date value to current_date
UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id = 1;