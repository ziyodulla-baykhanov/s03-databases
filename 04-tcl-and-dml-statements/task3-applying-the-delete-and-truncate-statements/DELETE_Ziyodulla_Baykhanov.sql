-- Task No. 3. Applying the DELETE and TRUNCATE Statements

-- (1.1) Remove all rental records, which correspond to the previously inserted film
-- The film_id of 'The Hobbit: An Unexpected Journey' is 1001
DELETE FROM public.rental
WHERE inventory_id IN (
  SELECT inventory_id FROM inventory
  WHERE film_id = 1001
);


-- (1.2) Remove the previously inserted film from the inventory
DELETE FROM public.inventory
WHERE film_id = 1001;


-- (2.1) Remove any records related to me (as a customer) from the payment table
-- My customer_id = 1
DELETE FROM public.payment
WHERE customer_id = 1;


-- (2.2) Remove any records related to me (as a customer) from the rental table
DELETE FROM public.rental
WHERE customer_id = 1;