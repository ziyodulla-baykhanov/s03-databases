-- Task No. 1. Applying the INSERT Statement

-- (1) Insert a film into the "film" table

INSERT INTO public.film
	("title", "description", "release_year", "language_id", "rental_duration", "rental_rate", "length", "replacement_cost", "rating", "last_update", "special_features", "fulltext")
VALUES
	('The Hobbit: An Unexpected Journey', 'A reluctant Hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home, and the gold within it from the dragon Smaug.', 2012, 1, 14, 4.99, 169, 19.99, 'PG-13', CURRENT_TIMESTAMP, ARRAY['Trailers','Commentaries','Behind the Scenes'], to_tsvector('The Hobbit: An Unexpected Journey'));


-- (2.1) Insert actors into the "actor" table

INSERT INTO public.actor
	("first_name", "last_name")
VALUES
	('MARTIN', 'FREEMAN'),
	('IAN', 'MCKELLEN'),
	('RICHARD', 'ARMITAGE'),
	('KEN', 'STOTT'),
	('GRAHAM', 'MCTAVISH'),
	('WILLIAM', 'KIRCHER'),
	('JAMES', 'NESBITT'),
	('STEPHEN', 'HUNTER'),
	('DEAN', 'O''GORMAN'),
	('AIDAN', 'TURNER'),
	('JOHN', 'CALLEN'),
	('PETER', 'HAMBLETON'),
	('JED', 'BROPHY'),
	('MARK', 'HADLOW'),
	('ADAM', 'BROWN');


-- (2.2) Insert film_actor relationships into the "film_actor" table

INSERT INTO public.film_actor ("actor_id", "film_id")
SELECT actor_id, film_id FROM actor
CROSS JOIN (VALUES (1001)) AS films(film_id)
WHERE actor_id BETWEEN 201 AND 215;


-- (3) Insert inventory records into the "inventory" table

INSERT INTO public.inventory
	("film_id", "store_id")
VALUES
	(1001, 1);
