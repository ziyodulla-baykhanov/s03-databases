# Overview of the "sh" Database

The "sh" database is a sample database that contains information about sales, costs, customers, products, promotions, and other related data. It consists of the following components:

- 9 tables: sh.channels, sh.costs, sh.countries, sh.customers, sh.products, sh.sales, sh.promotions, sh.supplementary_demographics, and sh.times;
- 1 view: sh.profits.

The tables have different relationships with each other:

- sh.supplementary_demographics is standalone table, it is not connected to any other tables;
- sh.sales has one-to-many relations with sh.customers, sh.products, sh.channels, sh.promotions, and sh.times;
- sh.costs has one-to-many relations with sh.products, sh.channels, sh.promotions, and sh.times;
- sh.customers table has one-to-many relation with sh.countries;
- sh.profits is view based on sh.sales and sh.costs tables.

The sh.profits view is derived from the sh.sales and sh.costs tables, which are the fact tables in this database. The other tables are dimension tables that provide descriptive attributes for the fact tables.

# Star Schema Design

A star schema is a data warehouse design that organizes data into a single large fact table and several smaller dimension tables. The fact table contains the transactional or measured data, while the dimension tables contain the attributes or characteristics of the data. The star schema is named after its shape, which resembles a star with the fact table at the center and the dimension tables radiating from it.

To create a star schema from the "sh" database, I performed the following steps:

- I renamed the sh.sales and sh.costs tables as sh.fact_sales and sh.fact_costs, respectively, to indicate that they are the fact tables in the star schema;
- I renamed the other tables by adding the preffix "dim_" to indicate that they are the dimension tables in the star schema.
- I merged the sh.customers and sh.countries tables into a single table called sh.dim_customers_and_countries, which contains the customer and country information for each sale;

# Snowflake Schema Design

A snowflake schema is a variation of the star schema, where some of the dimension tables are further normalized into sub-dimension tables. This reduces the redundancy and size of the dimension tables, but also increases the complexity and number of joins required to query the data. The snowflake schema is named after its shape, which resembles a snowflake with the fact table at the center and the dimension tables branching out into sub-dimension tables.

To create a snowflake schema from the "sh" database, I performed the following steps:

- I split the sh.dim_customers table into three sub-dimension tables: sh.subdim_financials, sh.subdim_contacts, and sh.subdim_addresses, which contain the financial, contact, and address information of the customers, respectively;
- I further split the sh.subdim_addresses table into a sub-dimension table called sh.subdim_countries, which contains the country information of the addresses;
- I split the sh.dim_products table into two sub-dimension tables: sh.subdim_suppliers and sh.subdim_categories, which contain the supplier and category information of the products, respectively;
- I further split the sh.subdim_categories table into a sub-dimension table called sh.subdim_subcategories, which contains the subcategory information of the categories;
- I split the sh.dim_promotions table into two sub-dimension tables: sh.subdim_promo_totals and sh.subdim_promo_categories, which contain the total and category information of the promotions, respectively;
- I split the sh.dim_times table into eight sub-dimension tables: sh.subdim_days, sh.subdim_weeks, sh.subdim_calendar_months, sh.subdim_calendar_quarters, sh.subdim_calendar_years, sh.subdim_fiscal_months, sh.subdim_fiscal_quarters, and sh.subdim_fiscal_years, which contain the day, week, month, quarter, and year information of the times, respectively.
