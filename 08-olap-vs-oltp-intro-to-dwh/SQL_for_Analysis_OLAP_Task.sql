-- Task 1: Retrieve the total sales amount for each product category for a specific time period.
SELECT
    prod_category AS "product_category", 
    SUM(s.amount_sold) AS "total_sales_amount"
FROM
    sh.sales s
    JOIN sh.products p ON s.prod_id = p.prod_id
    JOIN sh.times t ON s.time_id = t.time_id
WHERE
    t.time_id BETWEEN '1998-01-01' AND '2001-12-31' -- for any time period between 1998-01-01 and 2001-12-31
GROUP BY
    prod_category
ORDER BY
    total_sales_amount DESC;


-- Task 2: Calculate the average sales quantity by region for a particular product.
SELECT
    c.country_region AS "region",
    AVG(s.quantity_sold) AS "average_sales_quantity"
FROM
    sh.sales s
    JOIN sh.customers cust ON s.cust_id = cust.cust_id
    JOIN sh.countries c ON cust.country_id = c.country_id
WHERE
    s.prod_id = FLOOR(RANDOM() * (48-13+1) + 13)::int -- for any product ID in the ranges: 13-48, 113-148
GROUP BY
    c.country_region
ORDER BY
    average_sales_quantity DESC;


-- Task 3: Find the top five customers with the highest total sales amount.
SELECT
    c.cust_id,
    CONCAT(c.cust_first_name, ' ', c.cust_last_name) AS "customer_name",
    SUM(s.amount_sold) AS "total_sales_amount"
FROM
    sh.sales s
    JOIN sh.customers c ON s.cust_id = c.cust_id
GROUP BY
    c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY
    total_sales_amount DESC
LIMIT 5;

    