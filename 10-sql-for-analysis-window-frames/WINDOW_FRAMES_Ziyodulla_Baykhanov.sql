/**
* Task Description:
* You need to construct a query that meets the following requirements:
* 1. Generate a sales report for the 49th, 50th, and 51st weeks of 1999.
* 2. Include a column named CUM_SUM to display the amounts accumulated during each week.
* 3. Include a column named CENTERED_3_DAY_AVG to show the average sales for the previous, 
*    current, and following days using a centered moving average:
*    - For Monday, calculate the average sales based on the weekend sales 
*      (Saturday and Sunday) as well as Monday and Tuesday.
*    - For Friday, calculate the average sales on Thursday, Friday, and the weekend.
* Ensure that your calculations are accurate for the beginning of week 49 and the end of week 51.
*/


-- Create a CTE to calculate the sales information for each day in the 49th, 50th, and 51st weeks of 1999
WITH sales_by_day AS (
    SELECT
        t.calendar_week_number,
        s.time_id,
        t.day_number_in_week,
        t.day_name,
        SUM(s.amount_sold) AS sales
    FROM
        sh.sales s
        JOIN sh.times t ON s.time_id = t.time_id
    WHERE
        -- Filter the rows by the calendar year and the calendar week number
        t.calendar_year = 1999 AND
        t.calendar_week_number IN (49, 50, 51)
    GROUP BY
        s.time_id,
        t.day_number_in_week,
        t.day_name,
        t.calendar_week_number
),
-- Create another CTE to calculate the cumulative sum of sales for each week 
-- and the centered 3-day average of sales
sales_by_week AS (
    SELECT
        calendar_week_number,
        time_id,
        day_number_in_week,
        day_name,
        sales,
        -- Use the window function SUM with the OVER clause to calculate the cumulative sum of sales for each week
        -- Partition the rows by the calendar week number and order them by the time id
        -- Use the ROWS clause to specify the frame of rows to be included in the calculation
        -- Use UNBOUNDED PRECEDING and CURRENT ROW to include all the rows from the start of the partition to the current row
        SUM(sales) OVER (
            PARTITION BY calendar_week_number
            ORDER BY time_id
            ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW
        ) AS cum_sum,
        -- Use the CASE expression to handle different scenarios for calculating the centered 3-day average of sales
        -- Depending on the day number in week, use different frames of rows to include in the calculation
        -- Use the window function AVG with the OVER clause to calculate the average of sales for the specified frame
        -- Order the rows by the time id
        CASE
            -- For Monday, use the previous two days (Saturday and Sunday) and the next day (Tuesday) to calculate the average
            WHEN day_number_in_week = 1 THEN AVG(sales) OVER (
                ORDER BY time_id
                ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING
            )
            -- For Friday, use the previous day (Thursday) and the next two days (Saturday and Sunday) to calculate the average
            WHEN day_number_in_week = 5 THEN AVG(sales) OVER (
                ORDER BY time_id
                ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING
            )
            -- For other days, use theprevious, current, and following days to calculate the average
            ELSE AVG(sales) OVER (
                ORDER BY time_id
                ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING
            )
        END AS centered_3_day_avg
    FROM
        -- Use the sales_by_day table as the source table
        sales_by_day
)
-- Select the final columns from the sales_by_week table
SELECT
    calendar_week_number,
    time_id,
    day_name,
    sales,
    cum_sum,
    -- Round the centered 3-day average to 2 decimal places
    ROUND(centered_3_day_avg, 2)
FROM
    sales_by_week
ORDER BY
    time_id;


/**
* Description of the query:
* This query is designed to generate a sales report for the 49th, 50th, and 51st weeks of 1999. 
* The report shows the sales amount, the cumulative sum of sales, 
* and the centered 3-day average of sales for each day in the specified weeks.
*
* - sales_by_day CTE: 
*   This CTE calculates the sales amount for each day in the specified weeks 
*   by joining the sales table with the times table on the time_id column. 
*   This allows us to access the time-related information for each sales transaction, 
*   such as the calendar week number, the day number in week, and the day name. 
*
*   The CTE also filters the rows by the calendar_year and the calendar_week_number columns, 
*   using the WHERE clause, to include only the sales data for the 49th, 50th, and 51st weeks of 1999. 
*   The CTE then groups the rows by the time id, the day number in week, the day name, 
*   and the calendar week number columns, using the GROUP BY clause, 
*   to aggregate the sales amount for each day in the specified weeks. 
*   The CTE selects the calendar_week_number, the time_id, the day_number_in_week, 
*   the day_name, and the aggregated sales amount as the output columns.
*
*
* - sales_by_week CTE: 
*   This CTE takes the output of the first CTE and adds two more columns: 
*   cum_sum and centered_3_day_avg. The cum_sum column uses the window function SUM 
*   with the OVER clause to calculate the cumulative sum of sales for each week. 
*   The OVER clause partitions the rows by the calendar week number column 
*   and orders them by the time id column. This means that the calculation is performed 
*   separately for each week and the rows are arranged in chronological order. 
*
*   The OVER clause also uses the ROWS clause to specify the frame of rows to be included 
*   in the calculation. The ROWS clause uses UNBOUNDED PRECEDING and CURRENT ROW to indicate 
*   that the frame starts from the first row of the partition and ends at the current row. 
*   This means that the cumulative sum of sales is calculated by adding the sales amount 
*   of the current row and all the previous rows in the same week.
*   The centered_3_day_avg column uses the window function AVG with the OVER clause 
*   and the CASE expression to calculate the average of sales for the previous, 
*   current, and following days using a centered moving average. 
*   The OVER clause orders the rows by the time id column, which means that 
*   the calculation is performed in chronological order. 
*
*   The OVER clause also uses the ROWS clause to specify the frame of rows to be included 
*   in the calculation. However, the frame size varies depending on the day number in week, 
*   which is handled by using a CASE expression. The CASE expression evaluates 
*   different scenarios for calculating the centered 3-day average of sales 
*   and returns the appropriate value for each scenario. The scenarios are as follows:
*
*     - For Monday, the frame size is four days, which includes the previous two days 
*       (Saturday and Sunday) and the next day (Tuesday). The ROWS clause uses 2 PRECEDING and 
*       1 FOLLOWING to indicate that the frame starts from the second previous row and ends at the next row. 
*       This means that the average of sales is calculated by adding the sales amount of the current row 
*       and the three adjacent rows, and dividing the sum by four.
*        
*     - For Friday, the frame size is also four days, which includes the previous day (Thursday) 
*       and the next two days (Saturday and Sunday). The ROWS clause uses 1 PRECEDING and 2 FOLLOWING 
*       to indicate that the frame starts from the previous row and ends at the second next row. 
*       This means that the average of sales is calculated by adding the sales amount of the current row 
*       and the three adjacent rows, and dividing the sum by four.
*
*     - For other days, the frame size is three days, which includes the previous day and the next day. 
*       The ROWS clause uses 1 PRECEDING and 1 FOLLOWING to indicate that the frame starts from the previous 
*       row and ends at the next row. This means that the average of sales is calculated by adding 
*       the sales amount of the current row and the two adjacent rows, and dividing the sum by three.
* 
* - Final SELECT statement: 
*   This statement selects the calendar_week_number, the time_id, the day_name, the sales amount, 
*   the cum_sum, and the centered_3_day_avg columns from the sales_by_week table.
*   The ROUND() function rounds the centered 3-day average of sales to 2 decimal places.
*   It also orders the data by the time id column.
*
* This query generates a sales report for the 49th, 50th, and 51st weeks of 1999, 
* with the additional columns to display the amounts accumulated during each week 
* and the average sales for the previous, current, and following days using a centered moving average.
*
* The query uses window functions SUM and AVG to perform calculations across a set of rows 
* that are related to the current row, without affecting the grouping or ordering of the result set. 
* The OVER clause in the window functions specifies how to partition, order, and frame the rows 
* for the calculation. The CASE expression in the second CTE handles different scenarios 
* for calculating the centered 3-day average of sales depending on the day number in week.
* 
* The query also shows how CTEs can be used to create temporary tables that store the intermediate results 
* of the query, making the query easier to understand and maintain.
*/
